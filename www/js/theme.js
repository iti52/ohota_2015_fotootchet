/**
 * Created on 06.11.15.
 */


jQuery(window).resize(function(){
    slider_init();
});

$(function() {
    slider_init();
});

function slider_init(){

    var vis = 3;

    if(jQuery(window).outerWidth() < 1190){
        vis = 2;
    }

    if(jQuery(window).outerWidth() < 780){
        vis = 1;
    }

    if (jQuery('.partner-slider').length){
        jQuery(".partner-slider").jCarouselLite({
            btnNext: ".partner-next",
            btnPrev: ".partner-prev",
            visible: vis
        });
    }
}